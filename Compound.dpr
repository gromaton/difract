program Compound;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  OptionsWin in 'OptionsWin.pas' {Form2},
  PatternName in 'PatternName.pas' {Form3},
  Windows,
  mxvectors in '..\..\MXLib\mxvectors.pas',
  mxarrays in '..\..\MXLib\mxarrays.pas',
  mxcommon in '..\..\MXLib\mxcommon.pas',
  mxcomplex in '..\..\MXLib\mxcomplex.pas',
  mxdisplay in '..\..\MXLib\mxdisplay.pas',
  mxlog in '..\..\MXLib\mxlog.pas',
  mxmetric in '..\..\MXLib\mxmetric.pas';

{$R *.RES}

begin
  SetThreadLocale(1049);
  Application.Initialize;
  Application.Title := 'Solid Solution';

  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
