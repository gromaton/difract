object Form2: TForm2
  Left = 410
  Top = 202
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Settings'
  ClientHeight = 310
  ClientWidth = 547
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 184
    Top = 280
    Width = 75
    Height = 25
    Caption = 'Yes'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 272
    Top = 280
    Width = 75
    Height = 25
    Caption = 'No'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 547
    Height = 273
    Align = alTop
    BevelInner = bvLowered
    Caption = 'Panel1'
    TabOrder = 2
    object Label1: TLabel
      Left = 208
      Top = 12
      Width = 75
      Height = 13
      Caption = 'Decimals format'
    end
    object Label2: TLabel
      Left = 250
      Top = 86
      Width = 47
      Height = 13
      Caption = 'Template:'
    end
    object Button3: TButton
      Left = 462
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Save'
      TabOrder = 0
      OnClick = Button3Click
    end
    object CheckBox1: TCheckBox
      Left = 8
      Top = 10
      Width = 145
      Height = 17
      Caption = 'Report autosave'
      TabOrder = 1
      OnClick = CheckBox1Click
    end
    object CheckBox2: TCheckBox
      Left = 8
      Top = 33
      Width = 169
      Height = 17
      Caption = 'Save request'
      TabOrder = 2
    end
    object CheckBox3: TCheckBox
      Left = 8
      Top = 56
      Width = 169
      Height = 17
      Caption = 'Load report on start'
      TabOrder = 3
    end
    object Memo1: TMemo
      Left = 2
      Top = 110
      Width = 543
      Height = 161
      Align = alBottom
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 4
      WantTabs = True
      WordWrap = False
      OnEnter = Memo1Enter
    end
    object Edit1: TEdit
      Left = 432
      Top = 8
      Width = 105
      Height = 21
      TabOrder = 5
      Text = '" "0.000;"-"0.000'
    end
    object ComboBox1: TComboBox
      Left = 300
      Top = 82
      Width = 145
      Height = 21
      Style = csDropDownList
      DropDownCount = 10
      ItemHeight = 13
      Sorted = True
      TabOrder = 6
      OnChange = ComboBox1Change
    end
    object Button4: TButton
      Left = 8
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Font...'
      TabOrder = 7
      OnClick = Button4Click
    end
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Left = 160
    Top = 464
  end
end
