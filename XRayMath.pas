unit XRayMath;

interface
uses  Math, CompMath, Dialogs, mxvectors, mxcommon;

type
  TOldKDO = record
    Step    : extended;
    NPoints : integer;
    Interval: extended;
    MaxInt  : extended;
    Pnts  : array[1..1000] of extended;
  end;
  TKDODat = array[1..2,1..1000] of extended;
type
  ChemSymb=record
    A,B,C: string[2];
  end;
  TLayer=record
    typ  : boolean;
    a    : extended;
    x    : extended;
    h    : extended;
    Mtl1 : integer;
    Mtl2 : integer;
    Symb : ChemSymb;
    chiHf: complex;
    chi0f: complex;
    Exx  : extended;
    Ezz  : extended;
  end;
  TMaterial=record
    Mtl  : integer;
    Symb : ChemSymb;
    chiHf: complex;
    chi0f: complex;
    a    : extended;
    nu   : extended;
  end;
  TDataBase=array[-1..10] of TMaterial;
  TStruct=array[0..10] of TLayer;

function Vegard(const Ax,B,X: extended): extended;

function dHKL(const a:extended; const h,k,l:integer): extended;
function BraggRad(const a,lambda: extended; const h,k,l: integer): extended;
function Bragg(const a,lambda: extended; const h,k,l: integer): extended;
function BraggSec(const a,lambda: extended; const h,k,l: integer): extended;

function TestOfReflection(const h,k,l: integer): Boolean;

function CalcEzz(const Exx,Asub,Aflm,nuf: extended): extended;

function FiZalegRad(const h,k,l, p,r,q: integer): extended;
function FiZaleg(const h,k,l, p,r,q: integer): extended;
function FiZalegSec(const h,k,l, p,r,q: integer): extended;

function SecToRad(const ArcSeconds: extended): extended; overload;
function SecToRad(const ArcSeconds: integer): extended; overload;
function RadToSec(const Radians: extended): extended;

procedure XRayDeform(const Wa,Wb,
                           h,k,l,
                           p,r,q: integer;
                     const a,lambda: extended;
                       var Exx,Ezz: extended);

procedure XRayDeformDiff(const Wa,Wb,
                               h,k,l,
                               p,r,q: integer;
                         const a,lambda: extended;
                           var Exx,Ezz: extended);

procedure Error_XRayDeform(const Wa,Wb,
                                 h,k,l,
                                 p,r,q: integer;
                           const a,lambda: extended;
                             var ErrExx,ErrEzz: extended);

procedure Error_XRayDeformDiff(const Wa,Wb,
                                     h,k,l,
                                     p,r,q: integer;
                               const a,lambda: extended;
                                 var ErrExx,ErrEzz: extended);

procedure XRayTriklinDeformDiff(const Wa,Wb,
                                      h,k,l,
                                      p,q,r: integer;
                                const a,lambda: extended;
                                  var Exx,Ezz: extended);

procedure XRayTriklinDeformDiff3(const Wa1,Wb1,Wa2,h1,k1,l1,h2,k2,l2,p,q,r: integer;
                                const a,lambda: extended;
                                  var Exx,Ezz,psi: extended);
procedure DirectTriklinModel4(Wa1,Wb1,Wa2,Wb2,h1,k1,l1,h2,k2,l2,p,q,r: integer;
                                const alat,lambda: extended;
                                  var Exx1,Ezz1,psi,psi2: extended);

procedure DirectTriklinMatchingModel4(Wa1,Wb1,Wa2,Wb2,h1,k1,l1,h2,k2,l2,p,q,r: integer;
                                const alat,lambda: extended;
                                Wa1ch,Wb1ch,Wa2ch,Wb2ch: Boolean;
                                  var Exx,Ezz,psi1,psi2: extended;
                                  var nWa1,nWa2,nWb1,nWb2: integer);

procedure CalcFilmCompound(const aSub,afX,af2,
                                 Exx,Ezz,
                                 nuf: extended;
                             var aFilm,X,Fmis,Relax: extended);
                             
procedure CalcFilmCompoundTenz(const aSub,afX,af2,Exx,Ezz,
                                 nuf: extended; h,k,l,p,r,q: integer;
                             var aFilm,X,Fmis,Relax: extended);

function WaveLenght(const Line:string): double;                             
function LatticeParameter(const Material:string): double;
function Puasson(const Material:string): double;

procedure TextToHKL(const Str: string;
                      var h,k,l: integer);
procedure StringToHKL(const Str: string; Prefix, Separator, Postfix: Char; var h,k,l: integer);

function StrToF(S: string): extended;

procedure EvaluteKDO(const S:TStruct;
                           p,r,q,
                           N,NT,
                           MinUng,
                           Lengh,
                           pad,
                           ai0:integer;
                           lambda:extended;
                       var DK:TKDODat;
                       var ier: integer);


implementation

uses sysutils;

{ ����� �� math.pas }

function ArcCos(X: Extended): Extended;
begin
  Result := ArcTan2(Sqrt(Abs(1 - X*X)), X);
end;

function ArcSin(X: Extended): Extended;
begin
  Result := ArcTan2(X, Sqrt(Abs(1 - X*X)))
end;

{=========================================}

function TestOfReflection(const h,k,l: integer): Boolean;
var sum: Word;
    chet: Boolean;
    i: Byte;
begin
 Result:=false;

  sum:=abs(h)+abs(k)+abs(l);

  if (h mod 2 =0)and(k mod 2 =0)and(l mod 2 =0) then chet:=true else chet:=false;
  if ((h mod 2 <>0)and(k mod 2 <>0)and(l mod 2 <>0))or((sum mod 4 =0)and(chet=true)) then Result:=true else Result:=false;

end;

function Vegard(const Ax,B,X: extended): extended;  // ������ ���������
begin                                             // �� ������ �������
  result:=Ax*X+B*(1-X);
end;

function dHKL(const a:extended; const h,k,l:integer): extended;
begin
  Result:=a/(sqrt(h*h+k*k+l*l));// �����. ����������
end;

function Bragg(const a,lambda:extended; const h,k,l:integer): extended;  // ���������� ��������
var d: extended;                                               // ���� ������ � ��������
begin                                                          // ��� -1, ����
  d:=a/(sqrt(h*h+k*k+l*l));// �����. ����������                // lambda > 2*d
  if lambda > 2*d then
    result:=-1
  else
    result:=arcsin(abs(lambda/(2*d)))*180/PI;
end;

function BraggRad(const a,lambda:extended; const h,k,l:integer): extended;  // ���������� ��������
var d: extended;                                               // ���� ������ � ��������
begin                                                          // ��� -1, ����
  d:=a/(sqrt(h*h+k*k+l*l));// �����. ����������                // lambda > 2*d
  if lambda > 2*d then
    result:=-1
  else
    result:=arcsin(abs(lambda/(2*d)));
end;

function BraggSec(const a,lambda:extended; const h,k,l:integer): extended;  // ���������� ��������
var d: extended;                                               // ���� ������ � ��������
begin                                                          // ��� -1, ����
  d:=a/(sqrt(h*h+k*k+l*l));// �����. ����������                // lambda > 2*d
  if lambda > 2*d then
    result:=-1
  else
    result:=arcsin(abs(lambda/(2*d)))*180*3600/PI;
end;

function CalcEzz(const Exx,Asub,Aflm,nuf: extended): extended;   // ���������� �������� Ezz
begin                                                      // Asub-�������� ��������
  result:=(1+nuf)/(1-nuf)*(aflm-asub)-2*nuf*Exx/(1-nuf);   // Aflm-�������� ������
end;                                                       // nuf- �-� �������� ������

function FiZalegRad(const h,k,l, p,r,q: integer): extended;         // ���������� ���� ���������
var
  t: extended;
begin                                                      // ��������� hkl � ���-�� prq
  t:=abs((h*p+k*r+l*q)/(sqrt(h*h+k*k+l*l)*sqrt(p*p+r*r+q*q)));
  result:=arccos(t); // � ��������
  if abs(result)<1e-7 then result:=0  ;
end;

function FiZaleg(const h,k,l, p,r,q: integer): extended;         // ���������� ���� ���������
begin
  result:=abs((h*p+k*r+l*q)/(sqrt(h*h+k*k+l*l)*sqrt(p*p+r*r+q*q)));                                                  // ��������� hkl � ���-�� prq
  result:=arccos(result) // � ��������
          *180/PI;
end;
function FiZalegSec(const h,k,l, p,r,q: integer): extended;         // ���������� ���� ���������
begin                                                      // ��������� hkl � ���-�� prq
  result:=arccos(abs((h*p+k*r+l*q)/(sqrt(h*h+k*k+l*l)*sqrt(p*p+r*r+q*q)))) // � ��������
          *180*3600/PI;
end;

function SecToRad(const ArcSeconds: extended): extended;
begin
  result:=ArcSeconds*PI/(180*3600);
end;
function SecToRad(const ArcSeconds: integer): extended;
begin
  result:=ArcSeconds*PI/(180*3600);
end;

function RadToSec(const Radians: extended): extended;
begin
  result:=Radians*180*3600/PI;
end;



procedure XRayDeform(const Wa,Wb,               // ���������� ����� ������, ���.���
                           h,k,l,               // ���������
                           p,r,q: integer;      // ���������� �����������
                     const a,lambda: extended;  // �������� ������� ��������, ������ �����
                       var Exx,Ezz: extended);  // ������������� ����������, %
var
  dTeta,dFi,Fi,Teta: extended;
begin
  dTeta:=SecToRad((Wa+Wb)/2);
  dFi:=SecToRad((Wb-Wa)/2);
  Fi:=FiZalegRad(h,k,l,p,r,q);
  Teta:=BraggRad(a,lambda,h,k,l);

  Ezz:=(cos(Fi)*sin(Teta))/(cos(Fi+dFi)*sin(Teta+dTeta))*100-100;

  If Fi<>0 then         // ������������� ������
    Exx:=(sin(Fi)*sin(Teta))/(sin(Fi+dFi)*sin(Teta+dTeta))*100-100;
end;

procedure XRayDeformDiff(const Wa,Wb,
                               h,k,l,
                               p,r,q: integer;
                         const a,lambda: extended;
                           var Exx,Ezz: extended);
var
  dTeta,dFi,Fi,Teta: extended;
begin
  dTeta:=SecToRad((Wa+Wb)/2);
  dFi  :=SecToRad((Wb-Wa)/2);
  Fi:=FiZalegRad(h,k,l,p,r,q);
  Teta:=BraggRad(a,lambda,h,k,l);

  Ezz:=(-dTeta/tan(Teta)+dFi*tan(Fi))*100;

  If Fi<>0 then         // ������������� ������
    Exx:=(-dTeta/tan(Teta)-dFi/tan(Fi))*100;

end;

procedure XRayTriklinDeformDiff(const Wa,Wb,
                                      h,k,l,
                                      p,q,r: integer;
                                const a,lambda: extended;
                                  var Exx,Ezz: extended);
var
  dTeta,dFi,Fi,Teta, A1,B1,C1,D1, r2q: extended;
begin
  dTeta:=SecToRad((Wa+Wb)/2);
  dFi  :=SecToRad((Wb-Wa)/2);
  Fi:=FiZalegRad(h,k,l,p,q,r);
  Teta:=BraggRad(a,lambda,h,k,l);
  B1:=-Tan(Teta)*(1-((q*q+r*r)*h*h+(p*p+r*r)*k*k+(q*q+p*p)*l*l-2*p*q*h*k-2*q*r*k*l-2*p*r*h*l)/((p*p+q*q+r*r)*(h*h+k*k+l*l)));
  if Fi<>0 then
  begin
    A1:=-Tan(Teta)*(2-((2*p*p+q*q+r*r)*h*h+(2*q*q+p*p+r*r)*k*k+(2*r*r+p*p+q*q)*l*l+2*p*q*h*k+2*q*r*k*l+2*p*r*h*l)/((p*p+q*q+r*r)*(h*h+k*k+l*l)));
    C1:=Power(p*p+q*q+r*r,-3/2)*Power(h*h+k*k+l*l,-3/2)*(p*h+q*k+r*l)*((2*p*p+q*q+r*r)*h*h+(2*q*q+p*p+r*r)*k*k+(2*r*r+p*p+q*q)*l*l+2*p*q*h*k+2*q*r*k*l+2*p*r*h*l)
       +2*Power(p*p+q*q+r*r,-1/2)*Power(h*h+k*k+l*l,-1/2)*(p*h+q*k+r*l)
       -2*Power(p*p+q*q+r*r,-3/2)*Power(h*h+k*k+l*l,-1/2)*((2*p*p+q*q+r*r)*p*h+(2*q*q+p*p+r*r)*q*k+(2*r*r+p*p+q*q)*r*l+p*q*(p*k+q*h)+p*r*(p*l+h*r)+q*r*(q*l+k*r));
    D1:=Power(p*p+q*q+r*r,-3/2)*Power(h*h+k*k+l*l,-3/2)*(p*h+q*k+r*l)*((q*q+r*r)*h*h+(p*p+r*r)*k*k+(q*q+p*p)*l*l-2*p*q*h*k-2*q*r*k*l-2*p*r*h*l);
    C1:=C1/sin(Fi);
    D1:=D1/sin(Fi);
    Exx:=100*(dTeta*D1-dFi*B1)/(A1*D1-B1*C1);
    Ezz:=100*(dFi*A1-dTeta*C1)/(A1*D1-B1*C1);
  end
  else
  begin
    Exx:=0;
    Ezz:=100*dTeta/B1;
  end;
end;

procedure XRayTriklinDeformDiff3(const Wa1,Wb1,Wa2,h1,k1,l1,h2,k2,l2,p,q,r: integer;
                                const a,lambda: extended;
                                  var Exx,Ezz,psi: extended);
var
  Fi,Teta, A1,B1,C1,D1,A2,B2,C2,D2, dWa1,dWb1,dWa2: extended;
begin
  //dWa1:=Wa1; dWb1:=Wb1; dWa2:=Wa2;
  dWa1:=SecToRad(Wa1);
  dWb1:=SecToRad(Wb1);
  dWa2:=SecToRad(Wa2);
  Teta:=BraggRad(a,lambda,h1,k1,l1);
  Fi:=FiZalegRad(h1,k1,l1,p,q,r);
  B1:=-Tan(Teta)*(1-((q*q+r*r)*h1*h1+(p*p+r*r)*k1*k1+(q*q+p*p)*l1*l1-2*p*q*h1*k1-2*q*r*k1*l1-2*p*r*h1*l1)/((p*p+q*q+r*r)*(h1*h1+k1*k1+l1*l1)));
  A1:=-Tan(Teta)*(2-((2*p*p+q*q+r*r)*h1*h1+(2*q*q+p*p+r*r)*k1*k1+(2*r*r+p*p+q*q)*l1*l1+2*p*q*h1*k1+2*q*r*k1*l1+2*p*r*h1*l1)/((p*p+q*q+r*r)*(h1*h1+k1*k1+l1*l1)));
  C1:=Power(p*p+q*q+r*r,-3/2)*Power(h1*h1+k1*k1+l1*l1,-3/2)*(p*h1+q*k1+r*l1)*((2*p*p+q*q+r*r)*h1*h1+(2*q*q+p*p+r*r)*k1*k1+(2*r*r+p*p+q*q)*l1*l1+2*p*q*h1*k1+2*q*r*k1*l1+2*p*r*h1*l1)
       +2*Power(p*p+q*q+r*r,-1/2)*Power(h1*h1+k1*k1+l1*l1,-1/2)*(p*h1+q*k1+r*l1)
       -2*Power(p*p+q*q+r*r,-3/2)*Power(h1*h1+k1*k1+l1*l1,-1/2)*((2*p*p+q*q+r*r)*p*h1+(2*q*q+p*p+r*r)*q*k1+(2*r*r+p*p+q*q)*r*l1+p*q*(p*k1+q*h1)+p*r*(p*l1+h1*r)+q*r*(q*l1+k1*r));
  D1:=Power(p*p+q*q+r*r,-3/2)*Power(h1*h1+k1*k1+l1*l1,-3/2)*(p*h1+q*k1+r*l1)*((q*q+r*r)*h1*h1+(p*p+r*r)*k1*k1+(q*q+p*p)*l1*l1-2*p*q*h1*k1-2*q*r*k1*l1-2*p*r*h1*l1);
  C1:=C1/sin(Fi);
  D1:=D1/sin(Fi);

  Teta:=BraggRad(a,lambda,h2,k2,l2);
  Fi:=FiZalegRad(h2,k2,l2,p,q,r);
  B2:=-Tan(Teta)*(1-((q*q+r*r)*h2*h2+(p*p+r*r)*k2*k2+(q*q+p*p)*l2*l2-2*p*q*h2*k2-2*q*r*k2*l2-2*p*r*h2*l2)/((p*p+q*q+r*r)*(h2*h2+k2*k2+l2*l2)));
  A2:=-Tan(Teta)*(2-((2*p*p+q*q+r*r)*h2*h2+(2*q*q+p*p+r*r)*k2*k2+(2*r*r+p*p+q*q)*l2*l2+2*p*q*h2*k2+2*q*r*k2*l2+2*p*r*h2*l2)/((p*p+q*q+r*r)*(h2*h2+k2*k2+l2*l2)));
  if Fi<>0 then
  begin
    C2:=Power(p*p+q*q+r*r,-3/2)*Power(h2*h2+k2*k2+l2*l2,-3/2)*(p*h2+q*k2+r*l2)*((2*p*p+q*q+r*r)*h2*h2+(2*q*q+p*p+r*r)*k2*k2+(2*r*r+p*p+q*q)*l2*l2+2*p*q*h2*k2+2*q*r*k2*l2+2*p*r*h2*l2)
         +2*Power(p*p+q*q+r*r,-1/2)*Power(h2*h2+k2*k2+l2*l2,-1/2)*(p*h2+q*k2+r*l2)
         -2*Power(p*p+q*q+r*r,-3/2)*Power(h2*h2+k2*k2+l2*l2,-1/2)*((2*p*p+q*q+r*r)*p*h2+(2*q*q+p*p+r*r)*q*k2+(2*r*r+p*p+q*q)*r*l2+p*q*(p*k2+q*h2)+p*r*(p*l2+h2*r)+q*r*(q*l2+k2*r));
    D2:=Power(p*p+q*q+r*r,-3/2)*Power(h2*h2+k2*k2+l2*l2,-3/2)*(p*h2+q*k2+r*l2)*((q*q+r*r)*h2*h2+(p*p+r*r)*k2*k2+(q*q+p*p)*l2*l2-2*p*q*h2*k2-2*q*r*k2*l2-2*p*r*h2*l2);
    C2:=C2/sin(Fi);
    D2:=D2/sin(Fi);
    Exx:=100*((dWa1+dWb1)*(B1+D1+B2-D2)-2*B1*(dWb1+dWa2))/(2*(A1*D1+A1*B2-A1*D2-B1*C1-B1*A2+B1*C2));
    Ezz:=100*((dWa1+dWb1)*(A1+C1+A2-C2)-2*A1*(dWb1+dWa2))/(2*(B1*C1+B1*A2-B1*C2-D1*A1-B2*A1+A1*D2));
  end
  else
  begin
    Exx:=100*((dWa1+dWb1)*(B1+D1+B2)-2*B1*(dWb1+dWa2))/(2*(A1*D1+A1*B2-B1*C1-B1*A2));
    Ezz:=100*((dWa1+dWb1)*(A1+C1+A2)-2*A1*(dWb1+dWa2))/(2*(B1*C1+B1*A2-D1*A1-B2*A1));
  end;
  psi:=dWb1-(Exx/100)*(A1+C1)-(Ezz/100)*(B1+D1);
  psi:=(Exx/100)*(A1-C1)+(Ezz/100)*(B1-D1)-dWa1;
  psi:=RadToDeg(psi);
end;

procedure DirectTriklinModel4(Wa1,Wb1,Wa2,Wb2,h1,k1,l1,h2,k2,l2,p,q,r: integer;
                                const alat,lambda: extended;
                                  var Exx1,Ezz1,psi,psi2: extended);
var
  Fi01,Fi02,Teta01,Teta02,dTeta1,dTeta2,dFi1,dFi2,d01,d02,
  F1,F2,F3,F4,G1,G2,D1,D2: Extended;
  tdFi1,tdFi2: Extended;
begin
  Fi01:=FiZalegRad(h1,k1,l1,p,q,r);
  Fi02:=FiZalegRad(h2,k2,l2,p,q,r);
  d01:=alat/sqrt(h1*h1+k1*k1+l1*l1);
  d02:=alat/sqrt(h2*h2+k2*k2+l2*l2);
  Teta01:=BraggRad(alat,lambda,h1,k1,l1);
  Teta02:=BraggRad(alat,lambda,h2,k2,l2);

  dTeta1:=SecToRad((Wa1+Wb1)/2);
  dTeta2:=SecToRad((Wa2+Wb2)/2);
  D1:=lambda/(2*sin(Teta01+dTeta1));
  D2:=lambda/(2*sin(Teta02+dTeta2));
  G1:=Fi01+SecToRad(-(Wa1-Wb1)/2);
  G2:=Fi02+SecToRad(-(Wa2-Wb2)/2);
  F1:=d01/(D1*cos(Fi01));
  F2:=d02/(D2*cos(Fi02));
  F3:=d01/(D1*sin(Fi01));
  F4:=d02/(D2*sin(Fi02));
  psi:=ArcTan((F1*cos(G1)-F2*cos(G2))/(F1*sin(G1)+F2*sin(G2)));
  psi2:=ArcTan(-(F3*sin(G1)-F4*sin(G2))/(F3*cos(G1)+F4*cos(G2)));

  {dFi1:=SecToRad((Wb1-Wa1)/2)+psi;
  dFi2:=SecToRad((Wb2-Wa2)/2)-psi;
  tdFi1:=SecToRad((Wb1-Wa1)/2)-psi2;
  tdFi2:=SecToRad((Wb2-Wa2)/2)+psi2;  }

  dFi1:=SecToRad(-(Wa1-Wb1)/2)+psi;
  dFi2:=SecToRad(-(Wa2-Wb2)/2)-psi2;
  Ezz1:=cos(Fi01)*D1/(cos(Fi01+dFi1)*d01)-1;
  Exx1:=sin(Fi02)*D2/(sin(Fi02+dFi2)*d02)-1;
  psi:=RadToDeg(psi);
  psi2:=RadToDeg(psi2);
  Ezz1:=Ezz1*100;
  Exx1:=Exx1*100;
end;

procedure DirectTriklinMatchingModel4(Wa1,Wb1,Wa2,Wb2,h1,k1,l1,h2,k2,l2,p,q,r: integer;
                                const alat,lambda: extended;
                                Wa1ch,Wb1ch,Wa2ch,Wb2ch: Boolean;
                                  var Exx,Ezz,psi1,psi2: extended;
                                  var nWa1,nWa2,nWb1,nWb2: integer);
var
  Fi01,Fi02,Teta01,Teta02,dTeta1,dTeta2,dFi1,dFi2,d01,d02,
  F1,F2,F3,F4,G1,G2,D1,D2,olddW1,olddW2: Extended;
  Chet,Shod: Boolean;
  Sign1,Sign2: ShortInt;
  Ninter: integer;
begin
  Fi01:=FiZalegRad(h1,k1,l1,p,q,r);
  Fi02:=FiZalegRad(h2,k2,l2,p,q,r);
  d01:=alat/sqrt(h1*h1+k1*k1+l1*l1);
  d02:=alat/sqrt(h2*h2+k2*k2+l2*l2);
  Teta01:=BraggRad(alat,lambda,h1,k1,l1);
  Teta02:=BraggRad(alat,lambda,h2,k2,l2);
  olddW1:=abs(Wa1-Wb1);
  olddW2:=abs(Wa2-Wb2);
  Chet:=true;
  Ninter:=0;
  if abs(Wa1-Wb1)>abs(Wa1-Wb1+1) then Sign1:=-1 else Sign1:=1;
  if abs(Wa2-Wb2)>abs(Wa2-Wb2+1) then Sign2:=-1 else Sign2:=1;
  repeat
    Inc(Ninter);
    if Ninter>15000 then begin ShowMessage('��������� ���������� ���������'); break; end;
    if Chet then
    begin
      if abs(Wa1)-abs(Wb1)=0 then Sign1:=-1*Sign1;
      if Wa1ch then Wa1:=Wa1-1*Sign1;
      if Wb1ch then Wb1:=Wb1+1*Sign1;
      Chet:=false;
    end
    else
    begin
      if abs(Wa2)-abs(Wb2)=0 then Sign2:=-1*Sign2;
      if Wa2ch then Wa2:=Wa2-1*Sign2;
      if Wb2ch then Wb2:=Wb2+1*Sign2;
      Chet:=true;
    end;
    dTeta1:=SecToRad((Wa1+Wb1)/2);
    dTeta2:=SecToRad((Wa2+Wb2)/2);
    D1:=lambda/(2*sin(Teta01+dTeta1));
    D2:=lambda/(2*sin(Teta02+dTeta2));
    G1:=Fi01+SecToRad(-(Wa1-Wb1)/2);
    G2:=Fi02+SecToRad(-(Wa2-Wb2)/2);
    F1:=d01/(D1*cos(Fi01));
    F2:=d02/(D2*cos(Fi02));
    F3:=d01/(D1*sin(Fi01));
    F4:=d02/(D2*sin(Fi02));
    psi1:=ArcTan(-(F1*cos(G1)-F2*cos(G2))/(F1*sin(G1)+F2*sin(G2)));
    psi2:=ArcTan((F3*sin(G1)-F4*sin(G2))/(F3*cos(G1)+F4*cos(G2)));
  until {(abs(psi1-psi2)<1e-5)}abs(RadToDeg(psi1-psi2)*3600)<1.5;

  dFi1:=SecToRad(-(Wa1-Wb1)/2)-psi1;
  Ezz:=cos(Fi01)*D1/(cos(Fi01+dFi1)*d01)-1;
  dFi2:=SecToRad(-(Wa2-Wb2)/2)+psi2;
  Exx:=sin(Fi02)*D2/(sin(Fi02+dFi2)*d02)-1;

  psi1:=RadToDeg(psi1);
  psi2:=RadToDeg(psi2);
  Ezz:=Ezz*100;
  Exx:=Exx*100;
  nWb1:=Wb1;
  nWb2:=Wb2;
  nWa1:=Wa1;
  nWa2:=Wa2;
end;

procedure Error_XRayDeform(const Wa,Wb,
                                 h,k,l,
                                 p,r,q: integer;
                           const a,lambda: extended;
                             var ErrExx,ErrEzz: extended);
var
  WaRad,WbRad,dTeta,dFi,Fi,Teta,DD: extended;
begin
  WaRad:=SecToRad(Wa);
  WbRad:=SecToRad(Wb);
  dTeta:=SecToRad((Wa+Wb)/2);
  dFi  :=SecToRad((Wb-Wa)/2);
  Fi:=FiZalegRad(h,k,l,p,r,q);
  Teta:=BraggRad(a,lambda,h,k,l);

  DD:=Power(WaRad*0.02,2)+Power(WbRad*0.02,2);
  ErrEzz:=Power(cos(Fi)*sin(Teta)/(sin(Fi+dFi)*sin(Teta+dTeta)),2)*0.25*DD+
          Power(cos(Fi)*sin(Teta)/(cos(Fi+dFi)*cos(Teta+dTeta)),2)*0.25*DD;
  ErrEzz:=100*sqrt(ErrEzz);

  ErrExx:=Power(sin(Fi)*sin(Teta)/(cos(Fi+dFi)*sin(Teta+dTeta)),2)*0.25*DD+
          Power(sin(Fi)*sin(Teta)/(cos(Fi+dFi)*cos(Teta+dTeta)),2)*0.25*DD;
  ErrExx:=100*sqrt(ErrExx);
end;

procedure Error_XRayDeformDiff(const Wa,Wb,
                                     h,k,l,
                                     p,r,q: integer;
                               const a,lambda: extended;
                                 var ErrExx,ErrEzz: extended);
var
  WaRad,WbRad,dTeta,dFi,Fi,Teta,DD: extended;
begin
  WaRad:=SecToRad(Wa);
  WbRad:=SecToRad(Wb);
  dTeta:=SecToRad((Wa+Wb)/2);
  dFi  :=SecToRad((Wb-Wa)/2);
  Fi:=FiZalegRad(h,k,l,p,r,q);
  Teta:=BraggRad(a,lambda,h,k,l);

  DD:=Power(WaRad*0.02,2)+Power(WbRad*0.02,2);

  ErrEzz:=0.5*sqrt(DD*(1/sqr(tan(Teta))+sqr(tan(Fi))))*100;
  ErrExx:=0.5*sqrt(DD*(1/sqr(tan(Teta))+1/sqr(tan(Fi))))*100;
end;

procedure CalcFilmCompound(const aSub,afX,af2,
                                 Exx,Ezz,
                                 nuf: extended;
                             var aFilm,X,Fmis,Relax: extended);
begin
  aFilm:=aSub*(100+(1-nuf)*Ezz/(1+nuf)+2*nuf*Exx/(1+nuf))/100;
  X:=(aFilm-af2)/(afX-af2);
  Fmis:=(aFilm-aSub)/aSub;
  Relax:=Exx/Fmis;
end;

procedure CalcFilmCompoundNew(const aSub,afX,af2,Exx1,Exx2,Ezz,
                                 nuf: extended;
                             var aFilm,X,Fmis,Relax: extended);
var Exx: Extended;
begin
  Exx:=(1-nuf)/(1+nuf)*Ezz/100+nuf/(1+nuf)*(Exx1/100+Exx2/100);
  aFilm:=aSub*(Exx+1);
  X:=(aFilm-af2)/(afX-af2);
  Fmis:=(aFilm-aSub)/aSub;
  Relax:=Exx/Fmis;
end;

procedure CalcFilmCompoundTenz(const aSub,afX,af2,Exx,Ezz,
                                 nuf: extended; h,k,l,p,r,q: integer;
                             var aFilm,X,Fmis,Relax: extended);
var
  Ncos: TVector; //������������ �������� �������, ����� ��������� �������� ��������� �������� ������,
                 //�� ������������ ��� ����� ����������� ���������� ��������� � ���������� ��������� (-1, 1, 0)
  A_per,A_par: extended; //������������ � ���������������� ������������� ����������
  f: extended;
begin
  NCos:=GetPlaneIntersection(GetVector(mx_IntToExt(h),mx_IntToExt(k),mx_IntToExt(l)),GetVector(-1,1,0));
  A_per:=aSub*(1+Ezz/100);
  A_par:=aSub*(1+Exx/100);
  f:=(Exx/100)*(sqr(Ncos.x)+sqr(Ncos.y))+(Ezz/100)*sqr(Ncos.z)-(4*sqrt(sqr(A_par)+sqr(A_per)))/(aSub*sqrt(3));
  aFilm:=aSub*(1+f);
  X:=(aFilm-af2)/(afX-af2);
  Fmis:=(aFilm-aSub)/aSub;
  Relax:=Exx/Fmis;
end;

function WaveLenght(const Line:string): double;
begin
   If Line=         'Ag Kalfa1' then Result:=StrToF('0.5594075')
   else if Line=    'Ag Kalfa2' then Result:=StrToF('0.563798')
   else if Line=    'Ag Kbeta1' then Result:=StrToF('0.497069')
   else if Line=    'Ag Kbeta2' then Result:=StrToF('0.497685')
   else if Line=    'Co Kalfa1' then Result:=StrToF('1.788965')
   else if Line=    'Co Kalfa2' then Result:=StrToF('1.792850')
   else if Line=    'Co Kbeta1' then Result:=StrToF('1.620790')
   else if Line=    'Cr Kalfa1' then Result:=StrToF('2.289700')
   else if Line=    'Cr Kalfa2' then Result:=StrToF('2.293606')
   else if Line=    'Cr Kbeta1' then Result:=StrToF('2.084870')
   else if Line=    'Cu Kalfa1' then Result:=StrToF('1.540562')
   else if Line=    'Cu Kalfa2' then Result:=StrToF('1.544398')
   else if Line=    'Cu Kbeta1' then Result:=StrToF('1.392218')
   else if Line=    'Fe Kalfa1' then Result:=StrToF('1.936042')
   else if Line=    'Fe Kalfa2' then Result:=StrToF('1.939980')
   else if Line=    'Fe Kbeta1' then Result:=StrToF('1.756610')
   else if Line=    'Ge Kalfa1' then Result:=StrToF('1.254054')
   else if Line=    'Ge Kalfa2' then Result:=StrToF('1.258011')
   else if Line=    'Ge Kbeta1' then Result:=StrToF('1.057300')
   else if Line=    'Ge Kbeta2' then Result:=StrToF('1.057830')
   else if Line=    'Mo Kalfa1' then Result:=StrToF('0.709300')
   else if Line=    'Mo Kalfa2' then Result:=StrToF('0.713590')
   else if Line=    'Mo Kbeta1' then Result:=StrToF('0.632288')
   else if Line=    'Mo Kbeta2' then Result:=StrToF('0.632860')
   else if Line=    'Ni Kalfa1' then Result:=StrToF('1.657910')
   else if Line=    'Ni Kalfa2' then Result:=StrToF('1.661747')
   else if Line=    'Ni Kbeta1' then Result:=StrToF('1.500135')
   else if Line=    'Zn Kalfa1' then Result:=StrToF('1.435155')
   else if Line=    'Zn Kalfa2' then Result:=StrToF('1.439000')
   else if Line=    'Zn Kbeta1' then Result:=StrToF('1.295250')
   else if Line=    'W Kalfa1' then Result:=StrToF('0.209')
   else if Line=    'W Kalfa2' then Result:=StrToF('0.2139')
   else if Line=    'W Kbeta1' then Result:=StrToF('0.1844')
   else Result:=0;
end;
{
Ag Kalfa1
Ag Kalfa2
Ag Kbeta1
Ag Kbeta2
Co Kalfa1
Co Kalfa2
Co Kbeta1
Cr Kalfa1
Cr Kalfa2
Cr Kbeta1
Cu Kalfa1
Cu Kalfa2
Cu Kbeta1
Fe Kalfa1
Fe Kalfa2
Fe Kbeta1
Ge Kalfa1
Ge Kalfa2
Ge Kbeta1
Ge Kbeta2
Mo Kalfa1
Mo Kalfa2
Mo Kbeta1
Mo Kbeta2
Ni Kalfa1
Ni Kalfa2
Ni Kbeta1
W Kalfa1
W Kalfa2
W Kbeta1
Zn Kalfa1
Zn Kalfa2
Zn Kbeta1
����  }

function LatticeParameter(const Material:string): double;
begin
   If Material=         'Si'   then Result:=5.428
   else if Material=    'Ge'   then Result:=5.657
   else if Material=    'GaAs' then Result:=5.653
   else if Material=    'GaP'  then Result:=5.451
   else if Material=    'InAs' then Result:=6.058
   else if Material=    'AlAs' then Result:=5.662
   else if Material=    'InP' then Result:=5.869
   else if Material=    'AlP' then Result:=5.463
   else if Material=    'AA' then Result:=5.653
   else if Material=    'AA' then Result:=5.653
   else Result:=0;
end;

function Puasson(const Material:string): double;
begin
   If Material=         'Si'   then Result:=0.278
   else if Material=    'Ge'   then Result:=0.272
   else if Material=    'GaAs' then Result:=0.311
   else if Material=    'GaP'  then Result:=0.307
   else if Material=    'InAs' then Result:=0.352
   else if Material=    'AlAs' then Result:=0.275
   else if Material=    'InP' then Result:=0.36
   else if Material=    'AlP' then Result:=0.0
   else if Material=    'AA' then Result:=0
   else if Material=    'AA' then Result:=0
   else if Material=    'Al' then Result:=0
   else if Material=    'AA' then Result:=0
   else Result:=0;
end;

procedure TextToHKL(const Str: string;
                      var h,k,l: integer);
var
  Err: Boolean;
  i,j,Sl: integer;
  S: array [0..6] of string;
begin
  h:=0;
  k:=0;
  l:=0;
  S[1]:='';
  S[2]:='';
  S[3]:='';
  S[4]:='';
  S[5]:='';
  S[6]:='';
  Err:=False;
  S[0]:=Trim(Str);
  Sl:=Length(S[0]);

  for i:=1 to Sl do
    if ((S[0][i]>='0') and (S[0][i]<='9')) or (S[0][i]='-') or (S[0][i]=' ') then begin  // ���� ������ ��������
      end
    else begin      // ���� ������ �� ��������
      Err:=True;
    end;

  if not Err then begin
    j:=1;
    i:=1;

    While i<=Sl do begin                                 // �������� ������ �� ��������
      if ((S[0][i]>='0') and (S[0][i]<='9')) then begin  // �������  - �����
        While (i<=Sl) and ((S[0][i]>='0') and (S[0][i]<='9')) do begin
          S[j]:=S[j]+S[0][i];
          i:=i+1;
        end;
        j:=j+1;
      end
      else begin                                          // ������� - ������ ��� ����
        While (i<=Sl) and ((S[0][i]=' ') or (S[0][i]='-')) do begin
          S[j]:=S[j]+S[0][i];
          i:=i+1;
        end;
        j:=j+1;
      end;
      If j>6 then i:=Sl+1;
    end;
    j:=j-1;
    if (j=1) and (Sl=3) then begin
      h:=StrToInt(S[1][1]);
      k:=StrToInt(S[1][2]);
      l:=StrToInt(S[1][3]);
      end
    else begin
      for i:=1 to j do begin          
        S[i]:=Trim(S[i]);
        if Length(S[i])>1 then
          if S[i][1]='-' then S[i]:='-';   // ������ �� '--'
      end;

      if (S[1]<>'') and (j=6) then begin
        h:=StrToInt(S[1]+S[2]);
        k:=StrToInt(S[3]+S[4]);
        l:=StrToInt(S[5]+S[6]);
      end;
      if ((S[1]<>'') and (S[1]<>'-')) and (j=5) then begin
        h:=StrToInt(S[1]);
        k:=StrToInt(S[2]+S[3]);
        l:=StrToInt(S[4]+S[5]);
      end;
    end;
  end;
end;

function StrToF(S: string): extended;
var
  i,l: integer;
begin
  l:=Length(S);
  for i:=1 to l do
    if (S[i]='.') or (S[i]=',') then S[i]:=DecimalSeparator;

  result:=StrToFloat(S);
end;

procedure StringToHKL(const Str: string; Prefix, Separator, Postfix: Char; var h,k,l: integer);
var
  i: Word;
  strVec: string;
begin
  i:=1;
  strVec:='';
  if Prefix<>'f' then
  begin
    While Str[i]<>Prefix do Inc(i);
    Inc(i);
  end;
  While Str[i]<>Separator do
  begin
    strVec:=strVec+Str[i];
    Inc(i);
  end;
  h:=StrToInt(strVec);
  strVec:='';
  Inc(i);
  While Str[i]<>Separator do
  begin
    strVec:=strVec+Str[i];
    Inc(i);
  end;
  k:=StrToInt(strVec);
  strVec:='';
  Inc(i);
  While (Str[i]<>Postfix)and(Str[i]<>#0) do
  begin
    strVec:=strVec+Str[i];
    Inc(i);
  end;
  l:=StrToInt(strVec);
end;

{##############################################################################}

procedure EvaluteKDO(const S:TStruct; p,r,q,N,NT,MinUng,Lengh,pad,ai0:integer;
                     lambda:extended; var DK:TKDODat; var ier: integer);
var
  i,j,k,NLayers,Nbuf,index: integer;
  tb,afa,g0,gH,b,c,dnorm,Step,BegUng,Ung,y111,y122,aj,rsl,ref: extended;
  chiHf,chi0f,y: array[0..100] of complex;                                    // !!!!!!!!!!!
  sumj,ampl,sumf,phij,pokexp,CVal,ExpVal: complex;
  hn,ss,h: array[0..100] of extended;

  function fs(const b,tb,phih,a,ex,ez,pad,c: extended): extended;
  var
    a1,a2,a3: extended;
  begin
    a1:=sqrt(abs(b))*sin(2*tb);
    a2:=ez*cos(a)*cos(a)+ex*sin(a)*sin(a);
    a3:=(ez-ex)*sin(a)*cos(a)*pad;
    fs:=(a1/(c*phih))*(a2*tan(tb)+a3);
  end;

begin
  for i:=1 to 1000 do begin
    DK[1,i]:=0;
    DK[2,i]:=0;
  end;
  for i:=0 to 100 do  begin
    y[i]:=CNul;
    chi0f[i]:=CNul;
    chiHf[i]:=CNul;
    ss[i]:=0;
  end;

  i:=1;
  while (i<=N) and (not S[i].typ) do
    Inc(i);
  Nbuf:=i-1;
  while (i<=N) do begin
    if not S[i].typ then begin
      ier:=1;
      Exit;
    end;
    Inc(i);
  end;

  tb:=DegToRad(Bragg(S[0].a,lambda,4,0,0));
  afa:=DegToRad(FiZaleg(4,0,0,p,r,q));

  g0:=sin(tb-afa);
  gH:=sin(tb+afa);
  b:=abs(g0/gH);
  c:=1;
  lambda:=lambda*1e-10;

  for i:=1 to N do begin
    dnorm:=pi*c*S[i].chiHf.r/(lambda*sqrt(abs(g0*gH)));
    hn[i]:=S[i].h*dnorm;
    ss[i]:=fs(b,tb,S[i].chiHf.r,afa,S[i].Exx,S[i].Ezz,pad,c);
    chiHf[i]:=S[i].chiHf;
    chi0f[i]:=S[i].chi0f;
    h[i]:=S[i].h;
  end;

  for i:=2 to NT do begin
    for j:=Nbuf+1 to N do begin
      index:=(N-Nbuf)*(i-1)+j;
      h[index]:=h[j];
      hn[index]:=hn[j];
      chiHf[index]:=chiHf[j];
      chi0f[index]:=chi0f[j];
      ss[index]:=ss[j];
    end;
  end;

  Nlayers:=(N-Nbuf)*NT+Nbuf;
  Step:=SecToRad(Lengh/901);
  BegUng:=tb+SecToRad(MinUng);

  for i:=0 to 900 do begin
    Ung:=BegUng+Step*i;
    for j:=1 to NLayers do begin
      y111:=2*b*sin(tb*2)*(Ung-tb)-(1+b)*chi0f[j].r;
      y122:=2*c*sqrt(abs(b))*chiHf[j].r;
      y[j]:=cmul(CRe1,ReToComp(y111/y122));
    end;
    sumj:=CNul;
    for j:=1 to NLayers do begin
      {  c          mu=(2.*pi/lambda)*chii0f(j,3)
         c          aj=0
         c          if(j.eq.m)then }
      aj:=1;
      {c          go to 15
       c          endif
       c          sumk=0
       c          do 10 k=j+1,m
       c10          sumk=sumk+d(k)
       c          aj=exp(-mu*((g0+abs(gH))/(2.*abs(g0*gH)))*sumk)
       15         continue}
      if j=1 then
        phij:=CNul
      else begin
        sumf:=CNul;
        for k:=1 to j-1 do
          sumf:=csum(sumf,cmul(ReToComp(hn[k]),csum(y[k],ReToComp(ss[k]))));
        phij:=cmul(sumf,ReToComp(2));
      end;
      CVal:=csum(y[j],ReToComp(ss[j]));
      if (CVal.r=0) and (CVal.i=0) then
        ampl:=ReToComp(hn[j])
      else
        ampl:=cdiv(csin(cmul(csum(y[j],ReToComp(ss[j])),
                    ReToComp(hn[j]))),csum(y[j],ReToComp(ss[j])));
      pokexp:=cmul(CIm1,csum(cmul(ReToComp(hn[j]),csum(y[j],
                         ReToComp(ss[j]))),phij));
      sumj:=csum(sumj,cmul(cmul(ReToComp(aj),cexp(pokexp)),ampl));
      ExpVal:=cexp(pokexp);
    end;  {of j }

    rsl:=cabs(cmul(cmul(CIm1,ReToComp(sqrt(b))),sumj));
    ref:=rsl*rsl*ai0;
    DK[1,i+1]:=Round(RadToSec(Ung)-abs(MinUng));
    DK[2,i+1]:=ref;
  end;
  ier:=0;
end;  {End of EvaluteKDO}

end.
