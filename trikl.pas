unit trikl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TForm1 = class(TForm)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure XRayTriklinDeformDiff(const Wa,Wb,
                                      h1,k1,l1,
                                      h2,k2,l2,
                                      p,q,r: integer;
                                const a,lambda: extended;
                                  var Exx,Ezz,psi: extended);

var
  Form1: TForm1;

implementation

procedure XRayTriklinDeformDiff(const Wa1,Wb1,Wa2,h1,k1,l1,h2,k2,l2,p,q,r: integer;
                                const a,lambda: extended;
                                  var Exx,Ezz,psi: extended);();
var
  Fi,Teta, A1,B1,C1,D1,A2,B2,C2,D2, r2q: extended;
begin
  Teta:=BraggRad(a,lambda,h1,k1,l1);
  Fi:=FiZalegRad(h1,k1,l1,p,q,r);
  B1:=-Tan(Teta)*(1-((q*q+r*r)*h1*h1+(p*p+r*r)*k1*k1+(q*q+p*p)*l1*l1-2*p*q*h1*k1-2*q*r*k1*l1-2*p*r*h1*l1)/((p*p+q*q+r*r)*(h1*h1+k1*k1+l1*l1)));
  A1:=-Tan(Teta)*(2-((2*p*p+q*q+r*r)*h1*h1+(2*q*q+p*p+r*r)*k1*k1+(2*r*r+p*p+q*q)*l1*l1+2*p*q*h1*k1+2*q*r*k1*l1+2*p*r*h1*l1)/((p*p+q*q+r*r)*(h1*h1+k1*k1+l1*l1)));
  C1:=Power(p*p+q*q+r*r,-3/2)*Power(h1*h1+k1*k1+l1*l1,-3/2)*(p*h1+q*k1+r*l1)*((2*p*p+q*q+r*r)*h1*h1+(2*q*q+p*p+r*r)*k1*k1+(2*r*r+p*p+q*q)*l1*l1+2*p*q*h1*k1+2*q*r*k1*l1+2*p*r*h1*l1)
       +2*Power(p*p+q*q+r*r,-1/2)*Power(h1*h1+k1*k1+l1*l1,-1/2)*(p*h1+q*k1+r*l1)
       -2*Power(p*p+q*q+r*r,-3/2)*Power(h1*h1+k1*k1+l1*l1,-1/2)*((2*p*p+q*q+r*r)*p*h1+(2*q*q+p*p+r*r)*q*k1+(2*r*r+p*p+q*q)*r*l1+p*q*(p*k1+q*h1)+p*r*(p*l1+h1*r)+q*r*(q*l1+k1*r));
  D1:=Power(p*p+q*q+r*r,-3/2)*Power(h1*h1+k1*k1+l1*l1,-3/2)*(p*h1+q*k1+r*l1)*((q*q+r*r)*h1*h1+(p*p+r*r)*k1*k1+(q*q+p*p)*l1*l1-2*p*q*h1*k1-2*q*r*k1*l1-2*p*r*h1*l1);
  C1:=C1/sin(Fi);
  D1:=D1/sin(Fi);

  Teta:=BraggRad(a,lambda,h2,k2,l2);
  Fi:=FiZalegRad(h2,k2,l2,p,q,r);
  B2:=-Tan(Teta)*(1-((q*q+r*r)*h2*h2+(p*p+r*r)*k2*k2+(q*q+p*p)*l2*l2-2*p*q*h2*k2-2*q*r*k2*l2-2*p*r*h2*l2)/((p*p+q*q+r*r)*(h2*h2+k2*k2+l2*l2)));
  A2:=-Tan(Teta)*(2-((2*p*p+q*q+r*r)*h2*h2+(2*q*q+p*p+r*r)*k2*k2+(2*r*r+p*p+q*q)*l2*l2+2*p*q*h2*k2+2*q*r*k2*l2+2*p*r*h2*l2)/((p*p+q*q+r*r)*(h2*h2+k2*k2+l2*l2)));
  C2:=Power(p*p+q*q+r*r,-3/2)*Power(h2*h2+k2*k2+l2*l2,-3/2)*(p*h2+q*k2+r*l2)*((2*p*p+q*q+r*r)*h2*h2+(2*q*q+p*p+r*r)*k2*k2+(2*r*r+p*p+q*q)*l2*l2+2*p*q*h2*k2+2*q*r*k2*l2+2*p*r*h2*l2)
       +2*Power(p*p+q*q+r*r,-1/2)*Power(h2*h2+k2*k2+l2*l2,-1/2)*(p*h2+q*k2+r*l2)
       -2*Power(p*p+q*q+r*r,-3/2)*Power(h2*h2+k2*k2+l2*l2,-1/2)*((2*p*p+q*q+r*r)*p*h2+(2*q*q+p*p+r*r)*q*k2+(2*r*r+p*p+q*q)*r*l2+p*q*(p*k2+q*h2)+p*r*(p*l2+h2*r)+q*r*(q*l2+k2*r));
  D2:=Power(p*p+q*q+r*r,-3/2)*Power(h2*h2+k2*k2+l2*l2,-3/2)*(p*h2+q*k2+r*l2)*((q*q+r*r)*h2*h2+(p*p+r*r)*k2*k2+(q*q+p*p)*l2*l2-2*p*q*h2*k2-2*q*r*k2*l2-2*p*r*h2*l2);
  C2:=C1/sin(Fi);
  D2:=D1/sin(Fi);


  Exx:=((Wa1+Wb1)*(B1+D1+B2-D2)-2*B1*(Wb1+Wa2))/(2*(A1*D1+A1*B2-A1*D2-B1*C1-B1*A2+B1*C2));
  Ezz:=((Wa1+Wb1)*(A1+C1+A2-C2)-2*A1*(Wb1+Wa2))/(2*(B1*C1+B1*A2-B1*C2-D1*A1-B2*A2+A1*D2));
 psi:=Wb1-Exx*(A1+C1)-Ezz*(B1+D1);
end;


{$R *.dfm}

end.
