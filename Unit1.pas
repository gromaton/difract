unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ToolWin, Menus, ExtCtrls, ImgList, XRayMath, FileCtrl,
  OptionsWin;

type
  TForm1 = class(TForm)
    StatusBar1: TStatusBar;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    ToolBar1: TToolBar;
    GroupBox4: TGroupBox;
    RichEdit1: TRichEdit;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    GroupBox1: TGroupBox;
    ComboBox1: TComboBox;
    Edit1: TEdit;
    ComboBox2: TComboBox;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    ComboBox3: TComboBox;
    GroupBox3: TGroupBox;
    ComboBox4: TComboBox;
    ComboBox5: TComboBox;
    GroupBox5: TGroupBox;
    ComboBox6: TComboBox;
    Label3: TLabel;
    GroupBox6: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    Edit3: TEdit;
    Edit4: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    ImageList1: TImageList;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    Edit5: TEdit;
    Label6: TLabel;
    Edit6: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    Button1: TButton;
    Label14: TLabel;
    Edit8: TEdit;
    Label15: TLabel;
    N12: TMenuItem;
    ToolButton5: TToolButton;
    Image3: TImage;
    Image4: TImage;
    PopupMenu1: TPopupMenu;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    N19: TMenuItem;
    N20: TMenuItem;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    N21: TMenuItem;
    ToolButton9: TToolButton;
    Image5: TImage;
    Image6: TImage;
    GroupBox7: TGroupBox;
    Edit7: TEdit;
    Label16: TLabel;
    GroupBox8: TGroupBox;
    ComboBox7: TComboBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    Edit9: TEdit;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    Label21: TLabel;
    procedure ComboBox1Change(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure ComboBox4Change(Sender: TObject);
    procedure ComboBox5Change(Sender: TObject);
    procedure GroupBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Button1Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure RichEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure N12Click(Sender: TObject);
    procedure Edit3Enter(Sender: TObject);
    procedure Edit3Exit(Sender: TObject);
    procedure Edit4Enter(Sender: TObject);
    procedure Edit4Exit(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N6Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure ComboBox6Change(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);




  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

var
  WorkPath, CurrentDoc, CurrentPattern: String;
  DigFormat: string ;
  ReportAutoSave,
  SaveReportQwery,
  LoadReportAtStart,
  ReflexSimmetry: Boolean;               // True, ����� ������� ������������

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
   Edit1.text:=floattostr(WaveLenght(ComboBox1.text));
end;

procedure TForm1.ComboBox2Change(Sender: TObject);
begin
  Edit2.text:=floattostr(LatticeParameter(ComboBox2.text));
end;

procedure TForm1.ComboBox4Change(Sender: TObject);
begin
  Edit5.text:=floattostr(LatticeParameter(ComboBox4.text));

end;

procedure TForm1.ComboBox5Change(Sender: TObject);
begin
  Edit6.text:=floattostr(LatticeParameter(ComboBox5.text));
  Edit8.text:=floattostr(Puasson(ComboBox5.text));
end;

procedure TForm1.GroupBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var

  Teta,Fi,AlfaA,AlfaB: extended;
  h,k,l,p,r,q: integer;
begin

  //TextToHKL(ComboBox6.Text,h,k,l);
  StringToHKL(ComboBox6.Text,'f',' ',' ',h,k,l);
  //TextToHKL(ComboBox3.Text,p,r,q);
  StringToHKL(ComboBox3.Text,'f',' ',' ',p,r,q);

  if (not ((h=0) and (k=0) and (l=0))) and (not ((p=0) and (r=0) and (q=0))) then begin



  Teta:=Bragg( StrToF(Edit2.Text),StrToF(Edit1.Text),h,k,l);
  Fi:=FiZaleg(h,k,l,p,r,q);

  AlfaA:=Teta-Fi;
  AlfaB:=Teta+Fi;

 // StatusBar1.Panels[0].Text:=

  GroupBox1.Hint:='���� ������ = '+
                  FloatToStrF(Teta,
                                    ffGeneral,
                                    5,3)+' ��������'+Chr(13)+
                  '�������: ('+ComboBox6.Text+')'+Chr(13)+
                  '���� ��������� = '+
                  FloatToStrF(Fi,
                                    ffGeneral,
                                    5,3)+' ��������'+Chr(13)+
                  '���� ���������� A = '+
                  FloatToStrF(AlfaA,
                                    ffGeneral,
                                    5,3)+' ��������'+Chr(13)+
                  '���� ���������� B = '+
                  FloatToStrF(AlfaB,
                                    ffGeneral,
                                    5,3)+' ��������'//+Chr(13)+
                                    ;
  end;

end;

procedure TForm1.Button1Click(Sender: TObject);
var
  h,k,l,h2,k2,l2,p,r,q,Wa,Wb,Wa2,Wb2, StrPos,StrLength: integer;
  Exx,Ezz,ErExx,ErEzz,aFilm,X,Fmis,Relax: extended;
  ExxD,EzzD,ErExxD,ErEzzD,aFilmD,XD,FmisD,RelaxD: extended;
  aFilmD3,XD3,RelaxD3,FmisD3,ExxTD,EzzTD,ExxTD2,EzzTD2,psi1,psi2,
    psi3,psi4: extended; //������ ��� ���������� ���������
  nWa1,nWa2,nWb1,nWb2: integer; //������ ��� ���������� ���������
  ConfigFile: TextFile;
  CurCfgString,StringToReport,CurVarStr: string;
  GetPsi: Boolean;

begin

  //TextToHKL(ComboBox6.Text,h,k,l);
  StringToHKL(ComboBox6.Text,'f',' ',' ',h,k,l);
  StringToHKL(ComboBox3.Text,'f',' ',' ',p,r,q);
  StringToHKL(ComboBox7.Text,'f',' ',' ',h2,k2,l2);
  //TextToHKL(ComboBox3.Text,p,r,q);

  if (not ((h=0) and (k=0) and (l=0))) and (not ((p=0) and (r=0) and (q=0))) then begin

  Wa:=StrToInt(Edit3.Text);           // ���������� ����� ������ �




  If not ReflexSimmetry then begin       // ��� ������������� ������

    Wb:=StrToInt(Edit4.Text);                    // ���������� ����� ������ �

    Error_XRayDeform(Wa,Wb,h,k,l,p,r,q,StrToF(Edit2.Text),
                               StrToF(Edit1.Text),
                               ErExx,ErEzz);
    Error_XRayDeformDiff(Wa,Wb,h,k,l,p,r,q,StrToF(Edit2.Text),
                               StrToF(Edit1.Text),
                               ErExxD,ErEzzD);
    end
  else begin
 {   ShowMessage('������ ������������ !'+Chr(13)+Chr(13)+
                '��������� ����� ����������'+Chr(13)+Chr(13)+
                ''+Chr(13)+Chr(13)+
                'w = (w_A+w_B)/2');   }
    Wb:=Wa;
    ErExxD:=0;
    ErEzzD:=0;
    ErExx:=0;
    ErEzz:=0;

    end;


  XRayDeform(Wa,Wb,h,k,l,p,r,q,StrToF(Edit2.Text),
                               StrToF(Edit1.Text),
                               Exx,Ezz);

  XRayDeformDiff(Wa,Wb,h,k,l,p,r,q,StrToF(Edit2.Text),
                               StrToF(Edit1.Text),
                               ExxD,EzzD);

  if CheckBox1.Checked then
  begin
    Wa2:=StrToInt(Edit7.Text);
    XRayTriklinDeformDiff3(Wa,Wb,Wa2,h,k,l,h2,k2,l2,p,r,q,StrToF(Edit2.Text),StrToF(Edit1.Text),ExxTD,EzzTD,psi1);
    Psi1:=Psi1*3600;
    GetPsi:=true;
  end
  else
  if CheckBox2.Checked then
  begin
    Wa2:=StrToInt(Edit7.Text);
    Wb2:=StrToInt(Edit9.Text);
    DirectTriklinModel4(Wa,Wb,Wa2,Wb2,h,k,l,h2,k2,l2,p,r,q,StrToF(Edit2.Text),StrToF(Edit1.Text),ExxTD,EzzTD,psi1,psi2);
    if CheckBox3.Checked or CheckBox4.Checked or CheckBox5.Checked or CheckBox6.Checked then
      DirectTriklinMatchingModel4(Wa,Wb,Wa2,Wb2,h,k,l,h2,k2,l2,p,r,q,StrToF(Edit2.Text),StrToF(Edit1.Text),CheckBox3.Checked,CheckBox4.Checked,CheckBox5.Checked,CheckBox6.Checked,ExxTD2,EzzTD2,psi3,psi4,nWa1,nWa2,nWb1,nWb2);
    Psi1:=Psi1*3600;
    Psi2:=Psi2*3600;
    Psi3:=Psi3*3600;
    Psi4:=Psi4*3600;
    GetPsi:=true;
    if CheckBox3.Checked or CheckBox4.Checked or CheckBox5.Checked or CheckBox6.Checked then
      CalcFilmCompound(StrToF(Edit2.Text),
                   StrToF(Edit5.Text),
                   StrToF(Edit6.Text),
                   ExxTD2,EzzTD2,
                   StrToF(Edit8.Text),
                   aFilmD3,XD3,FmisD3,RelaxD3)
    else
      CalcFilmCompound(StrToF(Edit2.Text),
                   StrToF(Edit5.Text),
                   StrToF(Edit6.Text),
                   ExxTD,EzzTD,
                   StrToF(Edit8.Text),
                   aFilmD3,XD3,FmisD3,RelaxD3); 
  end
  else
  begin
    XRayTriklinDeformDiff(Wa,Wb,h,k,l,p,r,q,StrToF(Edit2.Text),
                               StrToF(Edit1.Text),
                               ExxTD,EzzTD);
    GetPsi:=False;
  end;

  CalcFilmCompound(StrToF(Edit2.Text),
                   StrToF(Edit5.Text),
                   StrToF(Edit6.Text),
                   Exx,Ezz,
                   StrToF(Edit8.Text),
                   aFilm,X,Fmis,Relax);

  CalcFilmCompoundTenz(StrToF(Edit2.Text),
                   StrToF(Edit5.Text),
                   StrToF(Edit6.Text),
                   Exx,Ezz,
                   StrToF(Edit8.Text), h,k,l,p,r,q,
                   aFilmD,XD,FmisD,RelaxD);

  AssignFile(ConfigFile,ExtractFilePath(Application.ExeName)+CurrentPattern);
  Reset(ConfigFile);

  While not Eof(ConfigFile) do begin
    StringToReport:='';
    ReadLn(ConfigFile, CurCfgString);
    StrLength:=Length(CurCfgString);
    StrPos:=1;
    While StrPos<=StrLength do begin
      If CurCfgString[StrPos]='#' then begin
        StrPos:=StrPos+1;
        CurVarStr:='';
        while (((CurCfgString[StrPos]>='0') and (CurCfgString[StrPos]<='9')) or
               ((CurCfgString[StrPos]>='A') and (CurCfgString[StrPos]<='Z')) or
               ((CurCfgString[StrPos]>='_') and (CurCfgString[StrPos]<='z')))
                and (StrPos<=StrLength) do begin
          CurVarStr:=CurVarStr+CurCfgString[StrPos];
          StrPos:=StrPos+1;
        end;

        If CurVarStr='D' then           StringToReport:=StringToReport+DateToStr(Date)
        else if CurVarStr='T' then      StringToReport:=StringToReport+TimeToStr(Time)
        else if CurVarStr='Tab' then    StringToReport:=StringToReport+Chr(9)


        else if CurVarStr='Ezz' then    StringToReport:=StringToReport+FormatFloat(DigFormat,Ezz)
        else if CurVarStr='EzzD' then   StringToReport:=StringToReport+FormatFloat(DigFormat,EzzD)
        else
        If not ReflexSimmetry then   begin     // ��� ������������� ������
               if CurVarStr='Exx' then    StringToReport:=StringToReport+FormatFloat(DigFormat,Exx)
          else if CurVarStr='ExxD' then   StringToReport:=StringToReport+FormatFloat(DigFormat,ExxD)
          else if CurVarStr='ErExx' then  StringToReport:=StringToReport+FormatFloat(DigFormat,ErExx)
          else if CurVarStr='ErEzz' then  StringToReport:=StringToReport+FormatFloat(DigFormat,ErEzz)
          else if CurVarStr='ErExxD' then StringToReport:=StringToReport+FormatFloat(DigFormat,ErExxD)
          else if CurVarStr='ErEzzD' then StringToReport:=StringToReport+FormatFloat(DigFormat,ErEzzD)


          else if CurVarStr='X' then      StringToReport:=StringToReport+FormatFloat(DigFormat,X)
          else if CurVarStr='fMis' then   StringToReport:=StringToReport+FormatFloat(DigFormat,fMis)
          else if CurVarStr='Relax' then  StringToReport:=StringToReport+FormatFloat(DigFormat,Relax)
          else if CurVarStr='aFilm' then  StringToReport:=StringToReport+FormatFloat(DigFormat,aFilm)
          else if CurVarStr='XD' then     StringToReport:=StringToReport+FormatFloat(DigFormat,XD)
          else if CurVarStr='fMisD' then  StringToReport:=StringToReport+FormatFloat(DigFormat,fMisD)
          else if CurVarStr='RelaxD' then StringToReport:=StringToReport+FormatFloat(DigFormat,RelaxD)
          else if CurVarStr='aFilmD' then StringToReport:=StringToReport+FormatFloat(DigFormat,aFilmD)
          else if CurVarStr='w_B' then    StringToReport:=StringToReport+Edit4.text
        end ;

             if CurVarStr='XRay' then   StringToReport:=StringToReport+ComboBox1.text
        else if CurVarStr='Reflex' then StringToReport:=StringToReport+ComboBox6.text
        else if CurVarStr='Subst' then  StringToReport:=StringToReport+ComboBox2.text
        else if CurVarStr='OrntS' then  StringToReport:=StringToReport+ComboBox3.text
        else if CurVarStr='Film1' then  StringToReport:=StringToReport+ComboBox4.text
        else if CurVarStr='Film2' then  StringToReport:=StringToReport+ComboBox5.text
        else if CurVarStr='w_A' then    StringToReport:=StringToReport+Edit3.text
        else if (CurVarStr='w_A2')and((CheckBox1.Checked)or(CheckBox2.Checked)) then    StringToReport:=StringToReport+Edit7.text
        else if (CurVarStr='w_B2')and(CheckBox2.Checked) then    StringToReport:=StringToReport+Edit9.text
        else if CurVarStr='EzzTD' then  StringToReport:=StringToReport+FormatFloat(DigFormat,EzzTD)
        else if (CurVarStr='EzzTD2')and(CheckBox2.Checked) then  StringToReport:=StringToReport+FormatFloat(DigFormat,EzzTD2)
        else if CurVarStr='ExxTD' then  StringToReport:=StringToReport+FormatFloat(DigFormat,ExxTD)
        else if (CurVarStr='ExxTD2')and(CheckBox2.Checked) then  StringToReport:=StringToReport+FormatFloat(DigFormat,ExxTD2)
        else if (CurVarStr='Psi1')and(GetPsi) then StringToReport:=StringToReport+FormatFloat(DigFormat,Psi1)
        else if (CurVarStr='Psi2')and(CheckBox2.Checked) then StringToReport:=StringToReport+FormatFloat(DigFormat,Psi2)
        else if (CurVarStr='Psi3')and(CheckBox2.Checked) then StringToReport:=StringToReport+FormatFloat(DigFormat,Psi3)
        else if (CurVarStr='Psi4')and(CheckBox2.Checked) then StringToReport:=StringToReport+FormatFloat(DigFormat,Psi4)
        else if (CurVarStr='Psi1')and(not GetPsi) then StringToReport:=StringToReport+'��� ���� ������ �� ������������'
        else if (CurVarStr='newWa1')and(CheckBox2.Checked) then StringToReport:=StringToReport+'A: '+IntToStr(nWa1)
        else if (CurVarStr='newWb1')and(CheckBox2.Checked) then StringToReport:=StringToReport+'B: '+IntToStr(nWb1)
        else if (CurVarStr='newWa2')and(CheckBox2.Checked) then StringToReport:=StringToReport+'A2: '+IntToStr(nWa2)
        else if (CurVarStr='newWb2')and(CheckBox2.Checked) then StringToReport:=StringToReport+'B2: '+IntToStr(nWb2)
        else if (CurVarStr='aFilmD3') then StringToReport:=StringToReport+FormatFloat(DigFormat,aFilmD3)
        else if (CurVarStr='XD3') then StringToReport:=StringToReport+FormatFloat(DigFormat,XD3)
        else if (CurVarStr='RelaxD3') then StringToReport:=StringToReport+FormatFloat(DigFormat,RelaxD3)
        else if CurVarStr=' ' then
        else if CurVarStr='' then
        else if CurVarStr='' then
      end
      else begin
        StringToReport:=StringToReport+CurCfgString[StrPos];
        StrPos:=StrPos+1;
      end;
    end;
    RichEdit1.Lines.Add(StringToReport);
  end;
  //RichEdit1.Lines.Add('���������� ������:   Exx = '+FormatFloat(DigFormat,ExxTD)+'    Ezz = '+FormatFloat(DigFormat,EzzTD));
  CloseFile(ConfigFile);

  end; 
end;

procedure TForm1.N4Click(Sender: TObject);
begin
  RichEdit1.Clear;
  CurrentDoc:='';
end;

procedure TForm1.RichEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    StatusBar1.Panels[1].Text:='SelStart='+
                        IntToStr(RichEdit1.SelStart);
end;

procedure TForm1.N12Click(Sender: TObject);
begin
  if n12.Checked then begin
    n12.Checked:=False;
    ToolButton5.Down:=False;
    RichEdit1.HideSelection:=True;
    end
  else begin
    n12.Checked:=True;
    ToolButton5.Down:=True;
    RichEdit1.HideSelection:=False;

    RichEdit1.SelectAll;                         //  ��������� ������ � �����
    RichEdit1.SelStart:=RichEdit1.SelLength ;    //
    RichEdit1.SelLength:=0;                      //
  end;


end;

procedure TForm1.Edit3Enter(Sender: TObject);
begin
  if ReflexSimmetry then
    Image6.Visible:=True
  else
    Image4.Visible:=True;

end;

procedure TForm1.Edit3Exit(Sender: TObject);
begin
  Image4.Visible:=False;
  Image6.Visible:=False;
end;

procedure TForm1.Edit4Enter(Sender: TObject);
begin
  Image3.Visible:=True;
end;

procedure TForm1.Edit4Exit(Sender: TObject);
begin
  Image3.Visible:=False;
end;

procedure TForm1.PopupMenu1Popup(Sender: TObject);
begin
  If RichEdit1.SelLength=0 then begin
    N13.Enabled:=False;
    N15.Enabled:=False;
    end
  else begin
    N13.Enabled:=True;
    N15.Enabled:=True;
  end;

end;

procedure TForm1.N13Click(Sender: TObject);
begin
  RichEdit1.CopyToClipboard;
end;

procedure TForm1.N15Click(Sender: TObject);
begin
  RichEdit1.CutToClipboard;
end;

procedure TForm1.N14Click(Sender: TObject);
begin
  RichEdit1.PasteFromClipboard;
end;

procedure TForm1.N17Click(Sender: TObject);
begin
  RichEdit1.SelectAll;
  RichEdit1.CopyToClipboard;
  RichEdit1.SelStart:=RichEdit1.SelLength;
end;

procedure SaveSettings;
var
  PathToConfig: string;
  IniFile: TextFile;
begin
  if Form1.ComboBox1.Text='������' then Form1.ComboBox1.Text:='Cu Kalfa1';
  PathToConfig:=ExtractFilePath(Application.ExeName);

  AssignFile(IniFile,PathToConfig+'settings.ini');
  Rewrite(IniFile);


  WriteLn(IniFile, '���� �������� ��������� "������� �������"');
  WriteLn(IniFile, '');

  WriteLn(IniFile, '������� ������� ='+WorkPath);
  WriteLn(IniFile, '���������='+Form1.ComboBox1.Text);
  WriteLn(IniFile, '��������='+Form1.ComboBox2.Text);
  WriteLn(IniFile, '���������� ��������='+Form1.ComboBox3.Text);
  WriteLn(IniFile, '������ X='+Form1.ComboBox4.Text);
  WriteLn(IniFile, '������ 1-X='+Form1.ComboBox5.Text);
  WriteLn(IniFile, '�������='+Form1.ComboBox6.Text);
  WriteLn(IniFile, '�������2='+Form1.ComboBox7.Text);

  WriteLn(IniFile, '���������� �='+Form1.Edit3.Text);
  WriteLn(IniFile, '���������� �='+Form1.Edit4.Text);
  WriteLn(IniFile, '���������� A2='+Form1.Edit7.Text);
  WriteLn(IniFile, '���������� B2='+Form1.Edit9.Text);
  if Form1.CheckBox1.Checked then
    WriteLn (IniFile, '������=3')
  else if Form1.CheckBox2.Checked then
    WriteLn (IniFile, '������=4')
  else
    WriteLn (IniFile, '������=2');

  WriteLn(IniFile, '�������������=',Form1.N12.Checked);
  WriteLn(IniFile, '����������=',Form1.N21.Checked);
  WriteLn(IniFile, '����X=',Form1.Left);
  WriteLn(IniFile, '����Y=',Form1.Top);
  WriteLn(IniFile, '���� ������=',CurrentDoc);
  WriteLn(IniFile, '���� �������=',CurrentPattern);
  WriteLn(IniFile, '�������������� ������=',ReportAutoSave);
  WriteLn(IniFile, '������ �� ����������=',SaveReportQwery);
  WriteLn(IniFile, '��������� ����� ��� ������=',LoadReportAtStart);
  WriteLn(IniFile, '������ �����=',DigFormat);
  WriteLn(IniFile, '�����=',Form1.RichEdit1.Font.Name);
  WriteLn(IniFile, '������ ������=',Form1.RichEdit1.Font.Size);


  CloseFile(IniFile);
end;

procedure LoadSettings;
var
  PathToConfig, ConfigString, Parameter, Value : string;
  IniFile: TextFile;
  StrPos, StrLen: integer;
begin
  PathToConfig:=ExtractFilePath(Application.ExeName);
  AssignFile(IniFile,PathToConfig+'settings.ini');
  Reset(IniFile);

  While not Eof(IniFile) do begin
    ReadLn(IniFile, ConfigString);
    Parameter:='';
    StrLen:=Length(ConfigString);
    if StrLen=0 then continue;
    StrPos:=1;
    While (ConfigString[StrPos]<>'=') and (StrPos<=StrLen) do begin
      Parameter:=Parameter+ConfigString[StrPos];
      StrPos:=StrPos+1;
    end;
    StrPos:=StrPos+1;
    Value:='';
    for StrPos:=StrPos to StrLen do
      Value:=Value+ConfigString[StrPos];

    if Parameter='������� ������� ' then WorkPath:=Value
    else if Parameter='���������' then Form1.ComboBox1.Text:=Value
    else if Parameter='��������' then  Form1.ComboBox2.Text:=Value
    else if Parameter='���������� ��������' then Form1.ComboBox3.Text:=Value
    else if Parameter='������ X' then  Form1.ComboBox4.Text:=Value
    else if Parameter='������ 1-X' then Form1.ComboBox5.Text:=Value
    else if Parameter='�������' then Form1.ComboBox6.Text:=Value
    else if Parameter='�������2' then Form1.ComboBox7.Text:=Value
    else if Parameter='���������� �' then Form1.Edit3.Text:=Value
    else if Parameter='���������� �' then Form1.Edit4.Text:=Value
    else if Parameter='���������� A2' then Form1.Edit7.Text:=Value
    else if Parameter='���������� B2' then Form1.Edit9.Text:=Value
    else if Parameter='������' then
    begin
      if Value='3' then begin
        Form1.CheckBox1.Checked:=true;
        Form1.CheckBox1Click(Form1);
      end;
      if Value='4' then begin
        Form1.CheckBox2.Checked:=true;
        Form1.CheckBox2Click(Form1);
      end;
    end
    else if Parameter='�������������' then
      if Value='TRUE' then begin
        Form1.N12.Checked:=True;
        Form1.ToolButton5.Down:=True;
        Form1.RichEdit1.HideSelection:=False;
        end
      else begin
        Form1.N12.Checked:=False;
        Form1.ToolButton5.Down:=False;
        Form1.RichEdit1.HideSelection:=True;
      end
    else if Parameter='����X' then Form1.Left:=StrToInt(Value)
    else if Parameter='����Y' then Form1.Top:=StrToInt(Value)
    else if Parameter='���� ������' then CurrentDoc:=Value
    else if Parameter='���� �������' then CurrentPattern:=Value
    else if Parameter='������ �� ����������' then
      if Value='TRUE' then begin
        SaveReportQwery:=True;
        end
      else begin
        SaveReportQwery:=False;
      end
    else if Parameter='��������� ����� ��� ������' then
      if Value='TRUE' then begin
        LoadReportAtStart:=True;
        end
      else begin
        LoadReportAtStart:=False;
      end
    else if Parameter='������ �����' then DigFormat:=Value
    else if Parameter='�������������� ������' then
      if Value='TRUE' then begin
        ReportAutoSave:=True;
        end
      else begin
        ReportAutoSave:=False;
      end
    else if Parameter='�����' then Form1.RichEdit1.Font.Name:=Value
    else if Parameter='������ ������' then Form1.RichEdit1.Font.Size:=StrToInt(Value)
    else if Parameter='����������' then
      if Value='TRUE' then begin
        Form1.N21.Checked:=True;
        Form1.ToolButton7.Down:=True;
        Form1.FormStyle:=fsStayOnTop;
        end
      else begin
        Form1.N21.Checked:=False;
        Form1.ToolButton7.Down:=False;
        Form1.FormStyle:=fsNormal;
      end
    else if Parameter='' then
    else if Parameter='' then
    else if Parameter='' then ;
  end;
  CloseFile(IniFile);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  LoadSettings;

  ComboBox1.OnChange(Form1);
  ComboBox2.OnChange(Form1);
  ComboBox4.OnChange(Form1);
  ComboBox5.OnChange(Form1);
  ComboBox6.OnChange(Form1);
  RichEdit1.Clear;

  If LoadReportAtStart then
    if CurrentDoc<>'' then RichEdit1.Lines.LoadFromFile(WorkPath+CurrentDoc);

  RichEdit1.SelectAll;
  RichEdit1.SelStart:=RichEdit1.SelLength ;
  RichEdit1.SelLength:=0;

  {Edit7.Enabled:=false;
  Edit9.Enabled:=false;
  ComboBox7.Enabled:=false;}
end;

procedure TForm1.N8Click(Sender: TObject);
begin
  Close;
end;


procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  If ReportAutoSave then Form1.N6Click(Sender)
  else
    if SaveReportQwery then
      if Application.MessageBox('��������� �����?',
                              '�������������', MB_OKCANCEL)=idOk  then
        Form1.N6Click(Sender);

  SaveSettings;      
end;

procedure TForm1.N7Click(Sender: TObject);
begin
  If SaveDialog1.Execute then begin
    if ExtractFileExt(SaveDialog1.FileName)='' then
      SaveDialog1.FileName:=SaveDialog1.FileName+'.rtf';
    RichEdit1.Lines.SaveToFile(SaveDialog1.FileName);
    WorkPath:=ExtractFilePath(SaveDialog1.FileName);
    CurrentDoc:=ExtractFileName(SaveDialog1.FileName);
  end;
end;

procedure TForm1.N6Click(Sender: TObject);
begin
  if (CurrentDoc<>'') and DirectoryExists(WorkPath) then
    RichEdit1.Lines.SaveToFile(WorkPath+CurrentDoc)
  else
   Form1.N7Click(Sender);
end;

procedure TForm1.N5Click(Sender: TObject);
begin
  OpenDialog1.InitialDir:=WorkPath;
  If OpenDialog1.Execute then begin
    RichEdit1.Lines.LoadFromFile(OpenDialog1.FileName);
    WorkPath:=ExtractFilePath(OpenDialog1.FileName);
    CurrentDoc:=ExtractFileName(OpenDialog1.FileName);

  end;
end;

procedure TForm1.N20Click(Sender: TObject);
var
  i: integer;
begin
  Application.CreateForm(TForm2, Form2);


  Delete(CurrentPattern, Length(CurrentPattern)-3, 4);
  i:=0;
  While (Form2.ComboBox1.items[i]<>CurrentPattern) and (i<Form2.ComboBox1.items.Count) do
    i:=i+1;
  Form2.ComboBox1.ItemIndex:=i;
  CurrentPattern:=CurrentPattern+'.cfg';

  Form2.CheckBox1.Checked:=ReportAutosave;
  Form2.CheckBox2.Checked:=SaveReportQwery;
  Form2.CheckBox3.Checked:=LoadReportAtStart;
  If ReportAutosave then Form2.CheckBox2.Enabled:=False;

  Form2.Memo1.Font:=RichEdit1.Font;

  If Form2.ShowModal = mrOK then begin
    RichEdit1.Font:=Form2.Memo1.Font;
    CurrentPattern:=Form2.ComboBox1.Text+'.cfg';

    ReportAutosave:=Form2.CheckBox1.Checked;
    SaveReportQwery:=Form2.CheckBox2.Checked;
    LoadReportAtStart:=Form2.CheckBox3.Checked;

    DigFormat:=Form2.Edit1.Text;
  end;

  Form2.Close;
end;

procedure TForm1.N18Click(Sender: TObject);
begin
  Form1.N20Click(Sender);
end;

procedure TForm1.N11Click(Sender: TObject);
begin
  ShowMessage('Solid Solution Compound'+Chr(13)+Chr(13)+'Version 3.1 (eng)  ');
end;

procedure TForm1.N10Click(Sender: TObject);
begin
  ShowMessage('��... ���� ����.  ����� �������.'+Chr(13)+Chr(13)+
  '������ �������� ���������� ��� ������� ���� � ������� "Default"');
end;

procedure TForm1.N21Click(Sender: TObject);
begin
  if n21.Checked then begin
    n21.Checked:=False;
    ToolButton7.Down:=False;
    Form1.FormStyle:=fsNormal;
    end
  else begin
    n21.Checked:=True;
    ToolButton7.Down:=True;
    Form1.FormStyle:=fsStayOnTop;
  end;

  RichEdit1.SelectAll;                         //  ��������� ������ � �����
  RichEdit1.SelStart:=RichEdit1.SelLength ;    //
  RichEdit1.SelLength:=0;                      //

end;

function Simmetry: Boolean;
var
  h,k,l,p,r,q: integer;
begin
  //TextToHKL(Form1.ComboBox6.Text,h,k,l);
  StringToHKL(Form1.ComboBox6.Text,'f',' ',' ',h,k,l);
  //TextToHKL(Form1.ComboBox3.Text,p,r,q);
  StringToHKL(Form1.ComboBox3.Text,'f',' ',' ',p,r,q);

  if (not ((h=0) and (k=0) and (l=0))) and (not ((p=0) and (r=0) and (q=0))) then begin


  If FiZalegRad(h,k,l,p,r,q)=0 then  begin      // ������������ ������
    Form1.Edit4.Visible:=False;
    Form1.Label5.Visible:=False;
    Form1.Label4.Visible:=False;
    Form1.Label13.Visible:=False;
    Form1.Image1.Visible:=False;
    Form1.Image2.Visible:=False;
    Form1.Image4.Visible:=False;
    Form1.Image5.Visible:=True;
    Form1.Image6.Visible:=False;
    Simmetry:=True;
    end
  else begin
    Form1.Edit4.Visible:=True;
    Form1.Label5.Visible:=True;
    Form1.Label4.Visible:=True;
    Form1.Label13.Visible:=True;
    Form1.Image1.Visible:=True;
    Form1.Image2.Visible:=True;
    Form1.Image4.Visible:=False;
    Form1.Image5.Visible:=False;
    Form1.Image6.Visible:=False;
    Simmetry:=False;
    end;

  end;
end;

procedure TForm1.ComboBox6Change(Sender: TObject);
begin
    ReflexSimmetry:=Simmetry;
end;

procedure TForm1.ComboBox3Change(Sender: TObject);
var
  p,r,q, sp,i: integer;
  L: byte;
begin
  L:=Length(ComboBox3.Text);
  sp:=0;
  for i:=0 to L-1 do
    if ComboBox3.Text[i]=' ' then Inc(sp);
  if (sp=2)and(ComboBox3.Text[L-1]<>' ') then  ReflexSimmetry:=Simmetry;
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  If {StrToF(}Edit1.Text{)}<>FloatToStr(WaveLenght(ComboBox1.Text)) then ComboBox1.text:='������';
end;

procedure TForm1.CheckBox1Click(Sender: TObject);
begin
  if CheckBox1.Checked then
  begin
    CheckBox2.Checked:=false;
    ComboBox7.Enabled:=true;
    Edit7.Enabled:=true;
    Edit9.Enabled:=false;
  end
  else
  begin
    ComboBox7.Enabled:=false;
    Edit7.Enabled:=false;
  end;
end;

procedure TForm1.CheckBox2Click(Sender: TObject);
begin
  if CheckBox2.Checked then
  begin
    CheckBox1.Checked:=false;
    ComboBox7.Enabled:=true;
    Edit7.Enabled:=true;
    Edit9.Enabled:=true;
  end
  else
  begin
    ComboBox7.Enabled:=false;
    Edit7.Enabled:=false;
    Edit9.Enabled:=false;
  end;
end;

Initialization
                                                



Finalization

end.
