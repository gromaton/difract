unit OptionsWin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls, PatternName;

type
  TForm2 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Panel1: TPanel;
    Button3: TButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    Memo1: TMemo;
    Edit1: TEdit;
    Label1: TLabel;
    ComboBox1: TComboBox;
    FontDialog1: TFontDialog;
    Button4: TButton;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Memo1Enter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.DFM}

var
  LastPattern: integer;

procedure TForm2.Button1Click(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TForm2.Button4Click(Sender: TObject);
begin
  FontDialog1.Font:=Memo1.Font;
  If FontDialog1.Execute then begin
    Memo1.Font:=FontDialog1.Font;
  end;
end;

procedure TForm2.FormCreate(Sender: TObject);
var
  AppPath: string;
  CfgFile: TSearchRec;
begin


  AppPath:=ExtractFilePath(Application.ExeName)+'*.cfg';
  If FindFirst(AppPath,faAnyFile,CfgFile)=0 then begin
    Delete(CfgFile.Name, Length(CfgFile.Name)-3, 4);
    ComboBox1.Items.Add(ExtractFileName(CfgFile.Name));
    While FindNext(CfgFile)=0 do begin
      Delete(CfgFile.Name, Length(CfgFile.Name)-3, 4);
      ComboBox1.Items.Add(ExtractFileName(CfgFile.Name));
    end;

    Button3.Enabled:=False;
  end;

end;

procedure TForm2.ComboBox1Change(Sender: TObject);
begin
  Memo1.Lines.LoadFromFile(ExtractFilePath(Application.ExeName)+ComboBox1.Text+'.cfg');
end;

procedure TForm2.Button3Click(Sender: TObject);
var
  i: integer;
begin
  Form3.ComboBox1.Items:=ComboBox1.Items;
  Form3.ComboBox1.ItemIndex:=ComboBox1.ItemIndex;
  If Form3.ShowModal=mrOk then begin
    If not FileExists(ExtractFilePath(Application.ExeName)+Form3.ComboBox1.Text+'.cfg') then
      begin
        ComboBox1.items.Add(Form3.ComboBox1.Text);
        i:=0;
        While (ComboBox1.items[i]<>Form3.ComboBox1.Text) and (i<ComboBox1.items.Count) do
          i:=i+1;
        ComboBox1.ItemIndex:=i;
      end
    else ComboBox1.ItemIndex:=Form3.ComboBox1.ItemIndex;

    Memo1.Lines.SaveToFile(ExtractFilePath(Application.ExeName)+Form3.ComboBox1.Text+'.cfg');
  end;

end;

procedure TForm2.Memo1Enter(Sender: TObject);
begin
  Button3.Enabled:=True;
end;

procedure TForm2.FormShow(Sender: TObject);
begin
  Form2.ComboBox1Change(Sender);
end;

procedure TForm2.CheckBox1Click(Sender: TObject);
begin
  CheckBox2.Enabled:= not CheckBox1.Checked;
end;

end.
